# Contributing to GPC

To contribute, simply create a merge request on the main
[GPC source code](https://gitlab.com/grouperenault/gitlab-project-configurator).

It won't be automatically merged in the source tree, but it will be cherry picked
and integrated in our internal base before being released back to the opensource
tree. The original authorship will be kept.
