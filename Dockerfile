FROM        python:3.10

COPY        dist/*.whl dist/prod-requirements.txt /opt/gpc/
COPY        gpc-default.cfg /opt/gpc/gpc.cfg

ENV         GPC_DEFAULTS_FILE=/opt/gpc/gpc.cfg

# Only install dependencies
RUN         find /opt/gpc/ \
        &&  pip install /opt/gpc/*.whl -r /opt/gpc/prod-requirements.txt \
        &&  echo "gpc --help:" \
        &&  gpc --help \
        &&  echo "gpc --version:" \
        &&  gpc --version \
        &&  rm -rf ~/.cache

CMD         [ "gpc" ]
